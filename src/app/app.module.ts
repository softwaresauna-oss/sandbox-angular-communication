import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DummyAComponent} from './dummy-a/dummy-a.component';
import {DummyBComponent} from './dummy-b/dummy-b.component';
import {DummyCommunicationService} from './dummy-communication.service';
import {DummyCComponent, ToLength} from './dummy-c/dummy-c.component';

@NgModule({
  declarations: [
    AppComponent,
    DummyAComponent,
    DummyBComponent,
    DummyCComponent,
    ToLength
  ],
  imports: [
    BrowserModule
  ],
  providers: [DummyCommunicationService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
