import { TestBed, inject } from '@angular/core/testing';

import { DummyCommunicationService } from './dummy-communication.service';

describe('DummyCommunicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DummyCommunicationService]
    });
  });

  it('should be created', inject([DummyCommunicationService], (service: DummyCommunicationService) => {
    expect(service).toBeTruthy();
  }));
});
