import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyCComponent } from './dummy-c.component';

describe('DummyCComponent', () => {
  let component: DummyCComponent;
  let fixture: ComponentFixture<DummyCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummyCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
