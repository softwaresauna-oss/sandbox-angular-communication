import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {DummyCommunicationService} from '../dummy-communication.service';

@Component({
  selector: 'app-dummy-c',
  templateUrl: './dummy-c.component.html',
  styleUrls: ['./dummy-c.component.css']
})
export class DummyCComponent implements OnInit {

  constructor(public dcs: DummyCommunicationService) { }

  ngOnInit() {
  }

}

@Pipe({name: 'toLength'})
export class ToLength implements PipeTransform {
  transform(value: number[]): number {

    return value.length;
  }
}
