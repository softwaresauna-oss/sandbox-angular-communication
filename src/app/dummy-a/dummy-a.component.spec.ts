import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyAComponent } from './dummy-a.component';

describe('DummyAComponent', () => {
  let component: DummyAComponent;
  let fixture: ComponentFixture<DummyAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummyAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
