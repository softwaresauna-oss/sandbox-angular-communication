import {Component, OnInit} from '@angular/core';
import {DummyCommunicationService} from '../dummy-communication.service';

@Component({
  selector: 'app-dummy-a',
  templateUrl: './dummy-a.component.html',
  styleUrls: ['./dummy-a.component.css']
})
export class DummyAComponent implements OnInit {

  constructor(private dcs: DummyCommunicationService) {
  }

  ngOnInit() {
  }

  increment() {

    this.dcs.increment();
  }

  generateArray() {

    this.dcs.generateArray();
  }
}
