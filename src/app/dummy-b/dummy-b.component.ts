import {Component, OnInit} from '@angular/core';
import {DummyCommunicationService} from '../dummy-communication.service';

@Component({
  selector: 'app-dummy-b',
  templateUrl: './dummy-b.component.html',
  styleUrls: ['./dummy-b.component.css']
})
export class DummyBComponent implements OnInit {

  constructor(public readonly dcs: DummyCommunicationService) {

  }

  ngOnInit() {
  }

}
