import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyBComponent } from './dummy-b.component';

describe('DummyBComponent', () => {
  let component: DummyBComponent;
  let fixture: ComponentFixture<DummyBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummyBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
