import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DummyCommunicationService {

  count = 0;

  numbers: number[] = [];

  constructor() {
  }

  increment() {

    this.count++;

  }

  generateArray() {

    const length = 10 + Math.floor(Math.random() * 10);

    const numbers = [];

    for (let i = 0; i < length; i++) {
      numbers.push(Math.floor(Math.random() * 100));
    }

    this.numbers = numbers;
  }
}

